﻿using System;

namespace HS.BuildUp.Core.Dto.Application
{
    public class TestDataDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string ContactNumber { get; set; }

        public string Address { get; set; }
    }
}
