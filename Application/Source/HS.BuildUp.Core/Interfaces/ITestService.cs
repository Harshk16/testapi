﻿using System;

namespace HS.BuildUp.Core.Interfaces
{
    public interface ITestService
    {
        void Add();

        void Edit();

        void DeleteAsync(Guid id);

        void GetDataAsync(Guid id);
    }
}
